import asyncio
import gc

from aiogram import Bot, Dispatcher, executor
from aiogram.types import Message

# Initialize bot and dispatcher
from requests_html import AsyncHTMLSession

loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)

bot = Bot(token="...")
dp = Dispatcher(bot, loop=loop)

with open("subscribers.txt", "r") as f:
    subscribers = set([int(x) for x in f.readlines()])

matches = {}


@dp.message_handler(regexp="/start")
async def handle(message: Message):
    chat_id = await message.from_user.id
    if chat_id not in subscribers:
        with open("subscribers.txt", "a") as f:
            f.write(f"{chat_id}\n")
        subscribers.add(chat_id)
    await message.answer("Вы успешно подписались")


async def get_updates(sleep_time: int):
    while True:
        try:
            print("Start")
            asession = AsyncHTMLSession()
            r = await asession.get("https://...")
            await r.html.arender(timeout=30)
            await asession.close()
            matchess = r.html.find(
                "body > "
                "div.container > "
                "div.container__content.content > "
                "div.container__main > "
                "div.container__mainInner > "
                "div.container__liveTableWrapper.sport_page > "
                "div.container__fsbody > "
                "div#live-table > "
                "div.event > "
                "div.leagues--live > "
                "div.sportName.soccer > "
                "div.event__match"
            )
            for match in matchess:
                event_stage = match.find("div.event__stage--block", first=True)
                if (
                    event_stage
                    and event_stage.text.isnumeric()
                ):
                    home = match.find(
                        "div.event__participant.event__participant--home", first=True
                    ).text
                    away = match.find(
                        "div.event__participant.event__participant--away", first=True
                    ).text
                    key = f"{home} - {away}"

                    if int(event_stage.text) > 15:
                        if matches.get(key):
                            del matches[key]
                    else:
                        score = match.find("div.event__scores.fontBold", first=True).text
                        h_score, a_score = map(int, score.split("-"))

                        prev_result = matches.get(key)

                        if prev_result is None:
                            matches[key] = {"h_score": int(h_score), "a_score": int(a_score)}
                        else:
                            new_score = {"h_score": int(h_score), "a_score": int(a_score)}
                            if prev_result["h_score"] < new_score["h_score"] == 2:
                                for sub in subscribers:
                                    await bot.send_message(
                                        sub, f"{home} забивает {away}, счет {home} {score} {away}"
                                    )
                            elif prev_result["a_score"] < new_score["a_score"] == 2:
                                for sub in subscribers:
                                    await bot.send_message(
                                        sub, f"{away} забивает {home}, счет {home} {score} {away}"
                                    )
                            matches[key] = new_score
            matchess = None
            r = None
            gc.collect()
            print("Done")
            await asyncio.sleep(sleep_time)
        except Exception as e:
            print(e)


dp.loop.create_task(get_updates(2))
executor.start_polling(dp, skip_updates=True)
